<?php
require_once("../../config/config.php");

function maintance($chat_id)
{
    global $maintance_msg;
    bot('sendmessage', [
        'chat_id' => $chat_id,
        'text' => $maintance_msg
    ]);
    exit();
}
