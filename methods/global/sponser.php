<?php
require_once("../../config/config.php");

// check if user joined active sponser channels
function validate_user_sponsers($user_id, $chat_id)
{
    global $db, $buttons, $admin_users_ids;
    $exit = false;

    $active_sponsers = $db->table("sponser")->select("username", "expire_at")->where([["expire_at", "<=", date("Y-m-d H:i:s")]])->execute();
    $bot_sponser_buttons = [];
    foreach ($active_sponsers as $sponser) {
        $sponser_username = $sponser['username'];
        array_push($bot_sponser_buttons, ['text' => "@$sponser_username", 'url' => "https://telegram.me/$sponser_username"]);
    }

    foreach ($active_sponsers as $sponser) {
        $sponser_username = $sponser['username'];
        $sponser_user_status = json_decode(file_get_contents("https://api.telegram.org/bot" . BOT_TOKEN . "/getChatMember?chat_id=@$sponser_username&user_id=" . $user_id));
        $sponser_user_status = $sponser_user_status->result->status;

        if ($sponser_user_status != 'member' && $sponser_user_status != 'creator' && $sponser_user_status != 'administrator' && !in_array($user_id, $admin_users_ids)) {
            $exit = true;
            bot('sendmessage', [
                'chat_id' => $chat_id,
                'text' => $buttons["sponser_require"],
                'parse_mode' => "html",
                'reply_markup' => json_encode(['inline_keyboard' => [$bot_sponser_buttons]])
            ]);
        }
    }

    if ($exit) {
        exit();
    }
}
