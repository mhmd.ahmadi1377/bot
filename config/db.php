<?php
require_once 'orm.php';

$hostname = "localhost";
$database = "test_db";
$username = "root";
$password = "";
$db = new ORM($hostname, $username, $password, $database);
$db->setLog(true, SCRIPT_FOLDER_PATH);