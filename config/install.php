<?php
require_once 'config.php';

$db->transaction();
try{
    $db->raw("CREATE TABLE `users` (`userid` int(32) PRIMARY KEY,`phone` bigint(12) NOT NULL,`command` text NOT NULL,`free_likes` int(5) NOT NULL,`free_followers` int(5) NOT NULL,`likes` int(5) NOT NULL,`followers` int(5) NOT NULL,`block` text NOT NULL)")->execute();
    $db->raw("CREATE TABLE `panel` (`power` text NULL)")->execute();
    $db->raw("SELECT * FROM `products`")->execute();
    $db->table('panel')->insert(["power"=>"ON"])->execute();
    $db->commit();
    create_folders();
    echo 'BOT IS READY TO GO!';
}catch (Exception $ex) {
    $db->rollback();
    if ($debug) {
        var_dump($ex->getMessage());
    }
}

function create_folders() {
    mkdir("data");
    mkdir("data/user");
    mkdir("data/code");
    mkdir("data/vpn");
    mkdir("data/vpn/v2ray");
    mkdir("data/vpn/ex");
}